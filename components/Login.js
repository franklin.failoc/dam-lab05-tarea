import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';

export default class Login extends Component {
  constructor() {
    super();
    this.state = {
      user: '',
      pass: '',
      message: '',
    };
  }

  ChangeUser = (text) => {
    this.setState({ user: text });
  };

  ChangePassword = (text) => {
    this.setState({ pass: text });
  };

  onPress = () => {
    this.setState({ message: '' });
    if (this.state.user == 'frank' && this.state.pass == 'frank') {
      this.textPassword.clear();
      this.textUsuario.clear();
      this.props.navigation.navigate('Listado');
    } else {
      this.textPassword.clear();
      this.textUsuario.clear();
      this.setState({
        message: 'contraseña o usuario incorrecto. Vuelva a intentar',
      });
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.image}>
          <Image
            style={styles.img_logo}
            source={require('../assets/login.png')}
          />
        </View>

        <View style={styles.text}>
          <Text style={styles.textLabel}>INISIAR SESION</Text>
        </View>

        <View style={styles.box2}>
          <Text style={styles.textLabel}> User </Text>
          <TextInput
            style={styles.textInput}
            ref={(input) => {
              this.textUsuario = input;
            }}
            onChangeText={(text) => this.ChangeUser(text)}
          />
        </View>

        <View style={styles.box2}>
          <Text style={styles.textLabel}>Password</Text>
          <TextInput
           
            ref={(input) => {
              this.textPassword = input;
            }}
            secureTextEntry={true}
            onChangeText={(text) => this.ChangePassword(text)}
            style={styles.textInput}
          />
        </View>

        <TouchableOpacity style={styles.button} onPress={this.onPress}>
          <Text style={styles.text_boton}> Ingresar </Text>
        </TouchableOpacity>

        <Text style={styles.message}> {this.state.message} </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 45,
  },

  text: {
    alignItems: 'center',
    padding: 10,
  },

  image: {
    alignItems: 'center',
  },

  img_logo: {
    width: 150,
    height: 150,
  },

  text_boton: {
    color: '#0A9492',
    fontWeight: 'bold',
  },

  button: {
    top: 8,
    alignItems: 'center',
    backgroundColor: '#ffdde1',
    borderRadius: 10,
    padding: 13,
    marginStart: 40,
    marginEnd: 40,
  },

  textInput: {
    paddingVertical: 4,
    marginHorizontal: 10,
    borderColor: '#ee9ca7',
    borderWidth: 1,
    borderRadius: 5,
  },

  textLabel: {
    marginBottom: 3,
    marginHorizontal: 10,
    color: '#0A9492',
    fontWeight: 'bold',
  },

  box2: {
    padding: 3,
  },
  message: {
    margin: 20
  }

});

//const { width: WIDTH } = Dimensions.get('window')
