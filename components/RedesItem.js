import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  Image,
} from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowCircleRight } from '@fortawesome/free-solid-svg-icons';



export default function RedesItem({ redes, onPress }) {
  return (
    <SafeAreaView style={styles.container}>
      <TouchableOpacity onPress={onPress}>
        <View style={styles.item}>
          <Image source={{ uri: redes.background_image }} style={styles.imagen} />
          <Text style={styles.txt_title}>{redes.title}</Text>

          <FontAwesomeIcon icon={faArrowCircleRight} style={styles.icon} />
        </View>
      </TouchableOpacity>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  imagen: {
    width: 50,
    height: 50,
  },
  item: {
    alignItems: 'center',
    backgroundColor: '#ee9ca7',
    padding: 20,
    marginVertical: 8,
    flexDirection: 'row',
    borderRadius: 15,
  },
  txt_title: {
    fontSize: 14,
    width:200,
    fontWeight: 'bold',
    marginStart: 10,

  },
  icon: {
    color: '#000',
    marginLeft: 'auto',
  },
});