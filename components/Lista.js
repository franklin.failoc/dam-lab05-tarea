import React, { Component, useEffect, useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TextInput,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import RedesItem from './RedesItem';

const DATA = [
  {
    id: 1,
    title: 'Facebook',
    background_image:
      'https://cdn-icons-png.flaticon.com/512/20/20673.png',
  },
  {
    id: 2,
    title: 'Whatsapp',
    background_image:
      'https://e7.pngegg.com/pngimages/994/990/png-clipart-whatsapp-computer-icons-message-facebook-whatsapp-logo-sms.png',
  },
  {
    id: 3,
    title: 'Messenger',
    background_image:
      'https://e7.pngegg.com/pngimages/518/671/png-clipart-social-media-facebook-messenger-computer-icons-desktop-social-media-angle-triangle-thumbnail.png',
  },
  {
    id: 4,
    title: 'Instagram',
    background_image:
      'https://w7.pngwing.com/pngs/513/241/png-transparent-computer-icons-icon-design-blog-instragram-desktop-wallpaper-area-symbol.png',
  },
];

export default function Lista({ navigation }) {
  const [data, setdata] = useState(DATA);

  useEffect(function () {
    async function fetchData() {
      const response = await fetch('https://yts.mx/api/v2/list_movies.json%27');
      const json = await response.json();
      setdata(json.data.movies);
    }
    fetchData();
  }, []);

 

  return (
    <View>
      <FlatList
        data={data}
        renderItem={({ item }) => (
          <RedesItem
            redes={item}
            onPress={() => navigation.navigate('Details', { itemObject: item })}
          />
        )}
        keyExtractor={(item) => DATA.id}
        ListHeaderComponent={
          <View style={styles.container_header}>
            <Text style={styles.lbl_header}> Lista De Redes</Text>
          </View>
        }
      />
    </View>
  );
}

const styles = StyleSheet.create({
  lbl_header: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  container_header: {
    flex: 1,
    alignItems: 'center',
  },
});