import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TextInput,
  TouchableOpacity,
} from 'react-native';

export default class Detalles extends Component {
  render() {
    const { params } = this.props.route;
    return (
      <View style={styles.container}>
        <Image
          style={styles.imagen}
          source={{ uri: params.itemObject.background_image }}
        />
        <Text style={styles.title}>{params.itemObject.title}</Text>
        <Text style={styles.description}>{params.itemObject.synopsis}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    padding: 10,
  },
  title: {
    fontSize: 24,
    padding: 5,
    color: '#b92b27',
  },
  imagen: {
    width: 250,
    height: 120,
    borderRadius: 10,
  },
  description: {
    color: '#000',
    marginTop: 5,
    fontSize: 16,
    borderStyle: 'solid',
    borderWidth:2,
    borderColor: '#000',
    padding:10,
    borderRadius:5
  },
});